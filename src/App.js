import {BrowserRouter, Route} from 'react-router-dom'
import {Home, Products, Blog} from 'pages';
import {Nav, Content} from 'components'

import MENU from 'data/menu.json'
import LOGO from 'images/logo.svg'

function App() {
  return (
    <BrowserRouter>
      <Nav logo={LOGO} data={MENU} />
      <Content>
          <Route exact path='/' component={Home} />
          <Route path='/product' component={Products} />
          <Route path='/blog' component={Blog} />
      </Content>
    </BrowserRouter>
  );
}

export default App;
