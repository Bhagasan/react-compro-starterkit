import React from 'react'
import st from './style.module.css'

export default function Button({type, className='', children, ...props}) {
  let style;
  switch(type){
    case 'icon': style=st.btnIcon; break;
    case 'link': style=st.btnLink; break;
    default: style=''; break;
  }

  return <button type='button' {...props} className={`${st.btn} ${style} ${className}`} {...props}>{children}</button>;
}
