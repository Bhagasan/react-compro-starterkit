import React from 'react'
import st from './Card.module.css'


export default function index({title, desc, id, href}) {
  return (
    <div className={`pointer ${st.card}`} id={id}>
      <h2>{title} &rarr;</h2>
      <p>{desc}</p>
    </div>
  )
}
