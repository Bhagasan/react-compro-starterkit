import React from 'react'
import st from './style.module.css'

export default function Content({children, className, ...props}) {
  return (
    <div {...props} className={`${st.container}`}>{children}</div>
  )
}
