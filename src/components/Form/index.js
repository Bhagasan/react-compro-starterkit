import React from 'react'
import st from './style.module.css';

export default function Form({children, title, ...props}) {
  return (
    <form {...props}>
      <div className={st.header}>{title && <h3>{title}</h3>}</div>
      {children}
    </form>
  )
}
