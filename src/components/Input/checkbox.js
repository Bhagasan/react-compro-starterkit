import React from 'react'
import st from './style.module.css'

export default function Checkbox() {
  return (
    <div>
      <input className={st.input} type='checkbox' />
    </div>
  )
}
