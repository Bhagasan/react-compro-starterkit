import React from 'react'
import st from './style.module.css'

import InputText from './inputText'
import InputPassword from './inputPassword'
import Textarea from './textarea'
import InputDate from './inputDate'
import InputEmail from './inputEmail'
import Radio from './radio'
import Checkbox from './checkbox'
import InputFile from './inputFile'
import InputSelect from './inputSelect'

export default function Input({type, className, label, id, errMsg, ...props}) {
  const elm = [];
  
  switch(type){
    case 'password': elm.push(<InputPassword id={id} key='haha-10' {...props} />); break;
    case 'textarea': elm.push(<Textarea id={id} key='haha-11' {...props} />); break;
    case 'radio': elm.push(<Radio id={id} key='haha-12' {...props} />); break;
    case 'email': elm.push(<InputEmail id={id} key='haha-13' {...props} />); break;
    case 'date': elm.push(<InputDate id={id} key='haha-14' {...props} />); break;
    case 'checkbox': elm.push(<Checkbox id={id} key='haha-15' {...props} />); break;
    case 'file': elm.push(<InputFile id={id} key='haha-16' {...props} />); break;
    case 'select': elm.push(<InputSelect id={id} key='haha-17' {...props} />); break;
    default: elm.push(<InputText id={id} key='haha-17' {...props} />); break;
  }

  return(
    <div className={`${st.container} ${className ? className : ''}`}>
      <label className={st.label} htmlFor={id}>{label}</label>
      {elm}
      {errMsg && <span className={st.errMsg}>{errMsg}</span> }
    </div>
  )
}
