import React from 'react'
import st from './style.module.css'

export default function InputDate() {
  return (
    <div>
      <input className={st.input} type='date' />
    </div>
  )
}
