import React from 'react'
import st from './style.module.css'

export default function InputEmail() {
  return (
    <div>
      <input className={st.input} type='email' />
    </div>
  )
}
