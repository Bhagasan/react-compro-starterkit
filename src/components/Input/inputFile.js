import React from 'react'
import st from './style.module.css'

export default function InputFile() {
  return (
    <div>
      <input className={st.input} type='file' />
    </div>
  )
}
