import React, {useState} from 'react'
import st from './style.module.css'

import {Button} from 'components';

export default function InputPassword({id, ...props}) {
  const [shown, setShow] = useState(false);
  return (
    <div className={st.fieldContainer}>
      <input id={id} {...props} className={`${st.input} ${st.pass}`} type={shown ? 'text' : 'password'} />
      <Button type='icon' onClick={()=>setShow(!shown)} className={st.btnAction} >{shown ? 'H' : 'S'}</Button>
    </div>
  )
}
