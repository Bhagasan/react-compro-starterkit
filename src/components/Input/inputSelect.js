import React, {useState} from 'react'
import st from './style.module.css'

export default function InputSelect({id, value, options, onClose, placeholder, ...props}) {
  const [isActive, setIsActive] = useState(false);

  const handleClose = param => {
    onClose(param);
    setIsActive(false);
  }

  const renderOptions = param => {
    const elm = [];
    if(param){
      param.forEach(d => elm.push(
        <div key={d.id} onClick={()=>handleClose(d.value)}>{d.label}</div>
      ))
    }
    return elm;
  }
  return (
    <>
      <button id={id} {...props} className={st.input} type="button" onClick={()=>setIsActive(!isActive)}>{value || placeholder}</button>
      {isActive && (
        <div className={st.popup}>
          {renderOptions(options)}
        </div>
      )}
    </>
  )
}
