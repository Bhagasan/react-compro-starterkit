import React from 'react'
import st from './style.module.css'

export default function InputText({id, ...props}) {
  return (
    <>
      <input id={id} {...props} className={st.input} type="text" />
    </>
  )
}
