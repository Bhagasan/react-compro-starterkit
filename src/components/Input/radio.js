import React from 'react'
import st from './style.module.css'

export default function Radio() {
  return (
    <div>
      <input className={st.input} type='radio' />
    </div>
  )
}
