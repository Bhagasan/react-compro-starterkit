import React from 'react'
import st from './style.module.css'

export default function Textarea() {
  return (
    <div>
      <textarea rows={5} className={st.input}></textarea>
    </div>
  )
}
