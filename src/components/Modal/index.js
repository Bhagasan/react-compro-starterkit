import {Button} from 'components';
import React, {useRef, useEffect} from 'react'
import st from './style.module.css'

export default function Modal({isActive = false, onClose, children, ...props}) {
  let ref = useRef();

  useEffect(()=>{
    document.addEventListener("mousedown", (event)=> {
      if(!ref.current.contains(event.target) && isActive){
        onClose();
      }
    })
  })
  return (
    <div {...props} className={`${st.wrapper} ${isActive ? st.active : ''}`}>
      <div ref={ref} className={st.panel}>
        {onClose && (
          <Button className={st.btnClose} onClick={onClose}>&times;</Button>
        )}
        {children}
      </div>
    </div>
  )
}
