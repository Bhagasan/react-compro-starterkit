import React from 'react'
import {NavLink, Link} from 'react-router-dom'
import st from './style.module.css'

export default function Nav({data, logo}) {
  const renderMenu = (data) => {
    return data.map((d, idx)=> (
      <div className="col" key={`nav-${idx}`}>
        <NavLink exact className={st.link} activeClassName='active' to={d.url}>{d.label}</NavLink>
      </div>
    ))
  }

  return (
    <nav className={`${st.nav}`}>
      <div className={`container-fluid h-100 d-flex align-items-center ${logo ? 'justify-content-between' : 'justify-content-end'}`}>
        {logo && <Link to='/'><img className={st.logo} src={logo} alt="logo" /></Link>}
        {data && (
          <div className="row">
            {renderMenu(data)}
          </div>
        )}
      </div>
    </nav>
  )
}
