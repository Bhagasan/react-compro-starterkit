import React from 'react'
import Plyr from 'plyr-react'
import 'plyr-react/plyr.css'
import st from './style.module.css'

import VIDEO from 'video/DjakartaWarehouseProject2022-DWP22-OfficialTrailer.mp4'

export default function Video() {
  const plyrProps = {
    source: {
      type: "video",
      sources: [
        {
          src: VIDEO,
          type: 'video/mp4',
          size: 720,
        }
      ]
    },
    options: {
      // controls: false
    },
  }
  return (
    <div>
      <h1>Video</h1>
      <div className={st.wrapper}>
        <div className={st.video}>
          <Plyr {...plyrProps} />
        </div>
      </div>
    </div>
  )
}
