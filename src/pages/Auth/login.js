import React from 'react'
import {Link} from 'react-router-dom';
import {Input, Button, Form} from '../../components';

import DATA_FORM from '../../data/login_form.json'

export default function Login() {

  const renderInput = (param) =>{
    const elm = [];
    if(param){
      param.forEach((d, idx) => {
        if(d.type === 'select'){
          
        }else{
          elm.push(
            <div className='col-12' key={`form_login-${idx}`} >
              <Input type={d.type} label={d.label} required={d.isRequired} />
            </div>
          )
        }
      });
    }else{
      elm.push(<span>Data not valid!</span>)
    }
    return elm;
  }


  return (
    <div className='row justify-content-center'>
      <div className="col-md-6">
        <Form title="Login">
          <div className="row">
            {renderInput(DATA_FORM)}
            <div className="col-12">
              <br/><hr /><br/>
              <div className="row">
                <div className="col-md-6"><Button>Login</Button></div>
                <div className="col-md-6 d-flex justify-content-end align-items-end"><Link to='/auth/register'>Register</Link></div>
              </div>
            </div>
          </div>
        </Form>
      </div>
    </div>
  )
}
