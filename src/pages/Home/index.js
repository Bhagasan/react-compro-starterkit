import React, {useState} from 'react'
import {Carousel, Modal, Video} from 'components'

export default function Home() {
  const [openModal, setOpenModal] = useState(false);
  return (
    <div className="container">
      <h1>Home</h1>
      <button onClick={()=> setOpenModal(true)}>Open Modal</button>
      <Video />
      <Modal isActive={openModal} onClose={()=>setOpenModal(false)}>
        <Carousel />
      </Modal>
    </div>
  )
}
