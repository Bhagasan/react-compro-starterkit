import Home from "./Home";
import Products from "./Product";
import Blog from "./Blog";

export {Home, Products, Blog};